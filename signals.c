void
setlayoutex(const Arg *arg)
{
	setlayout(&((Arg) { .v = &layouts[arg->i] }));
}

void
viewex(const Arg *arg)
{
	view(&((Arg) { .ui = 1 << arg->ui }));
}

void
viewall(const Arg *arg)
{
	view(&((Arg){.ui = ~0}));
}

void
toggleviewex(const Arg *arg)
{
	toggleview(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagex(const Arg *arg)
{
	tag(&((Arg) { .ui = 1 << arg->ui }));
}

void
toggletagex(const Arg *arg)
{
	toggletag(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagall(const Arg *arg)
{
	tag(&((Arg){.ui = ~0}));
}

void
mvplace_nw(const Arg *arg)
{
	moveplace(&((Arg){.ui = WIN_NW}));
}

void
mvplace_n(const Arg *arg)
{
	moveplace(&((Arg){.ui = WIN_N}));
}

void
mvplace_ne(const Arg *arg)
{
	moveplace(&((Arg){.ui = WIN_NE}));
}

void
mvplace_w(const Arg *arg)
{
	moveplace(&((Arg){.ui = WIN_W}));
}

void
mvplace_c(const Arg *arg)
{
	moveplace(&((Arg){.ui = WIN_C}));
}
void
mvplace_e(const Arg *arg)
{
	moveplace(&((Arg){.ui = WIN_E}));
}
void
mvplace_sw(const Arg *arg)
{
	moveplace(&((Arg){.ui = WIN_SW}));
}
void
mvplace_s(const Arg *arg)
{
	moveplace(&((Arg){.ui = WIN_S}));
}
void
mvplace_se(const Arg *arg)
{
	moveplace(&((Arg){.ui = WIN_SE}));
}
